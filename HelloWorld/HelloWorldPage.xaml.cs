﻿using Xamarin.Forms;

namespace HelloWorld
{
    public partial class HelloWorldPage : ContentPage
    {
        public HelloWorldPage()
        {
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            presentation.Text = "My name is Stessy MOUGEOT\nI'm major in computer science\nGraduate in 2019";
            discover.SetValue(IsVisibleProperty, false);
            picture.SetValue(IsVisibleProperty, true);
        }
    }
}
